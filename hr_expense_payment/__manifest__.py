# Copyright 2019 Tecnativa - Ernesto Tejeda
# Copyright 2021 Ecosoft Co., Ltd (http://ecosoft.co.th/)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

{
    "name": "HR Expense Payment",
    "version": "2.0.1.0.1",
    "category": "Human Resources",
    "author": "Tecnativa, Ecosoft, Odoo Community Association (OCA)",
    "license": "AGPL-3",
    "website": "https://gitlab.com/flectra-community/hr-expense",
    "depends": ["hr_expense"],
    "data": [],
    "installable": True,
    "post_init_hook": "post_init_hook",
}
