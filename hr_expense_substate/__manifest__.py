# Copyright 2021 Ecosoft (<http://ecosoft.co.th>)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

{
    "name": "Expense Report Sub State",
    "version": "2.0.1.0.1",
    "category": "Tools",
    "author": "Ecosoft,Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/hr-expense",
    "license": "AGPL-3",
    "depends": ["base_substate", "hr_expense"],
    "data": [
        "views/hr_expense_views.xml",
        "data/hr_expense_substate_mail_template_data.xml",
        "data/hr_expense_substate_data.xml",
    ],
    "demo": ["demo/hr_expense_substate_demo.xml"],
    "installable": True,
}
