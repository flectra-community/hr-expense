# Copyright 2021 Ecosoft
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html).

{
    "name": "HR Expense - Pay To Vendor",
    "version": "2.0.1.0.0",
    "author": "Ecosoft, Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/hr-expense",
    "license": "AGPL-3",
    "category": "Human Resources",
    "depends": ["hr_expense"],
    "data": ["views/hr_expense_views.xml"],
    "installable": True,
    "maintainers": ["kittiu"],
}
