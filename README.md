# Flectra Community / hr-expense

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[hr_expense_petty_cash](hr_expense_petty_cash/) | 2.0.1.0.1| Petty Cash
[hr_expense_sequence_option](hr_expense_sequence_option/) | 2.0.1.0.0| Manage sequence options for hr.expense.sheet
[hr_expense_advance_clearing](hr_expense_advance_clearing/) | 2.0.1.5.8| Employee Advance and Clearing
[hr_expense_sheet_payment_state](hr_expense_sheet_payment_state/) | 2.0.1.0.1| Backport of the Payment Status (payment_state field) in Expense Report which is introduced in Odoo Community Version 15.0
[hr_expense_payment](hr_expense_payment/) | 2.0.1.0.1| HR Expense Payment
[hr_expense_tier_validation](hr_expense_tier_validation/) | 2.0.1.2.1| Expense Tier Validation
[hr_expense_journal](hr_expense_journal/) | 2.0.1.0.0| Set the Journal for the payment type used to pay the expense
[hr_expense_substate](hr_expense_substate/) | 2.0.1.0.1| Expense Report Sub State
[hr_expense_advance_clearing_sequence](hr_expense_advance_clearing_sequence/) | 2.0.1.0.0| HR Expense Advance Clearing Sequence
[hr_expense_sequence](hr_expense_sequence/) | 2.0.1.0.2| HR expense sequence
[hr_expense_exception](hr_expense_exception/) | 2.0.1.0.1| Custom exceptions on expense report
[hr_expense_analytic_distribution](hr_expense_analytic_distribution/) | 2.0.1.0.1| HR Expense Analytic Distribution
[hr_expense_work_acceptance](hr_expense_work_acceptance/) | 2.0.1.0.0| Expense Work Acceptance
[hr_expense_invoice](hr_expense_invoice/) | 2.0.1.1.4| Supplier invoices on HR expenses
[hr_expense_payment_widget_amount](hr_expense_payment_widget_amount/) | 2.0.1.0.0| HR Expense Payment Widget Amount
[hr_expense_widget_o2m](hr_expense_widget_o2m/) | 2.0.1.0.0| HR Expense one2many widget
[hr_expense_advance_clearing_consolidation](hr_expense_advance_clearing_consolidation/) | 2.0.1.0.0|         HR Expense Advance Clearing Consolidation
[hr_expense_pay_to_vendor](hr_expense_pay_to_vendor/) | 2.0.1.0.0| HR Expense - Pay To Vendor
[hr_expense_due_date](hr_expense_due_date/) | 2.0.1.0.0|         Dedicated due date used for HR Expense Sheet
[hr_expense_cancel](hr_expense_cancel/) | 2.0.2.0.1| Hr expense cancel


