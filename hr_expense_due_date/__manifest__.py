# Copyright 2024 - TODAY, Escflectra
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).

{
    "name": "HR Expense Due Date",
    "summary": """
        Dedicated due date used for HR Expense Sheet""",
    "version": "2.0.1.0.0",
    "license": "AGPL-3",
    "author": "Escflectra,Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/hr-expense",
    "depends": [
        "hr_expense",
    ],
    "data": [
        "views/hr_expense_sheet.xml",
    ],
}
