# Copyright 2021 Ecosoft Co., Ltd (http://ecosoft.co.th/)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

{
    "name": "HR Expense Payment Widget Amount",
    "version": "2.0.1.0.0",
    "category": "Human Resources",
    "author": "Ecosoft, Odoo Community Association (OCA)",
    "license": "AGPL-3",
    "website": "https://gitlab.com/flectra-community/hr-expense",
    "depends": ["hr_expense_payment"],
    "data": ["views/hr_expense_views.xml"],
    "installable": True,
    "maintainers": ["Saran440"],
}
