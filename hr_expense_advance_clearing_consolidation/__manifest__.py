# Copyright 2022 - TODAY, Escflectra
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).

{
    "name": "Hr Expense Advance Clearing Consolidation",
    "summary": """
        HR Expense Advance Clearing Consolidation""",
    "version": "2.0.1.0.0",
    "license": "AGPL-3",
    "author": "Escflectra,Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/hr-expense",
    "depends": [
        "hr_expense_advance_clearing",
        "account_reconciliation_widget",
    ],
    "data": [
        "views/hr_expense_sheet_view.xml",
    ],
}
